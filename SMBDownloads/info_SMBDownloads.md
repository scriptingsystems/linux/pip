## SMB Downloads
**Fuente**: (https://github.com/SecureAuthCorp/impacket/blob/master/examples/smbserver.py)

The Server Message Block protocol (SMB protocol) that runs on port TCP/445 is common in enterprise networks where Windows services are running. It enables applications and users to transfer files to and from remote servers.

We can use SMB to download files from our Pwnbox easily. We need to create an SMB server in our Pwnbox with smbserver.py from Impacket and then use copy, move, PowerShell Copy-Item, or any other tool that allows connection to SMB.

```
[root@ansible-s1 PROJECT]# python3 smbserver.py -comment 'My Share' TMP /tmp                         
```

### Windows - Copy a File from the SMB Server
To download a file from the SMB server to the current working directory, we can use the following command:

```
C:\htb> copy \\192.168.220.133\share\nc.exe

        1 file(s) copied.
```



### Linux - Mount

```
[root@ansible-s2 ~]# mount.cifs //172.16.21.141/TMP /mnt --verbose -o guest,vers=1.0                 
mount.cifs kernel mount options: ip=172.16.21.141,unc=\\172.16.21.141\TMP,vers=1.0,user=,pass=*******   

[root@ansible-s2 ~]# mount | grep -i mnt
//172.16.21.141/TMP on /mnt type cifs (rw,relatime,vers=1.0,sec=none,cache=strict,uid=0,noforceuid,gid=0,noforcegid,addr=172.16.21.141,file_mode=0755,dir_mode=0755,soft,nounix,mapposix,rsize=16384,wsize=63936,bsize=1048576,echo_interval=60,actimeo=1)
```


## Create the SMB Server with a Username and Password

```
# python3 smbserver.py -comment 'My Share' TMP /tmp -username myuser -password mypassword
```

### Windows - Mount the SMB Server with Username and Password
```
C:\htb> net use n: \\192.168.220.133\share /user:test test

The command completed successfully.

C:\htb> copy n:\nc.exe
        1 file(s) copied.
```


### Linux - Mount the SMB Server with Username and Password
```
[root@ansible-s1 ~]# mount.cifs //localhost/TMP /mnt/SB --verbose -o vers=1.0,user=myuser
Password for myuser@//localhost/TMP: 
mount.cifs kernel mount options: ip=::1,unc=\\localhost\TMP,vers=1.0,user=myuser,pass=********
mount error(111): could not connect to ::1mount.cifs kernel mount options: ip=127.0.0.1,unc=\\localhost\TMP,vers=1.0,user=myuser,pass=********
```

